use actix_web::{web, App, HttpResponse, HttpServer};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().route("/health-check", web::get().to(health)))
        .bind("127.0.0.1:8080")?
        .run()
        .await
}

pub async fn health() -> HttpResponse {
    return HttpResponse::NoContent().finish();
}
